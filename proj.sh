#!/bin/bash

echo "Bem-vindo ao meu script de compartilhamento de arquivos via SCP!"

# Solicitar informações do usuário
read -p "Digite o nome de usuário e o endereço IP (user@ip): " user_id
read -p "Caminho do arquivo de origem: " caminho
read -p "Caminho de destino (para /onde_voce_quer): " destino

# Construir o comando SCP e realizar a transferência
scp "$user_id:$caminho" "$destino"

# Verificar o status da transferência
if [ $? -eq 0 ]; then
  echo "Arquivo transferido com sucesso para $user_and_ip:$destination_path"
else
  echo "Ocorreu um erro durante a transferência. Certifique-se de que o caminho está correto"
fi

