#!/bin/bash

echo "Bem-vindo ao meu script de compartilhamento de arquivos via SCP!"

# Solicitar informações do usuário
user_id=$(yad --entry --title="Nome do Usuário" --text="Digite o nome de usuário e o endereço IP (user@ip): ") 
caminho=$(yad --entry --title="Caminho do arquivo de origem: " --text="Digite /home/ifpb..." )
destino=$(yad --entry --title="Caminho de destino" --text="Digite para /onde_voce_quer: ")

# Construir o comando SCP e realizar a transferência
scp "$user_id:$caminho" "$destino"

# Verificar o status da transferência
if [ $? -eq 0 ]; then
  echo "Arquivo transferido com sucesso para $user_and_ip:$destination_path"
else
  echo "Ocorreu um erro durante a transferência. Certifique-se de que o caminho está correto"
fi

