#!/bin/bash

# Função para obter a confirmação do usuário
obter_confirmacao() {
    resposta=$(zenity --question --text="Deseja ir para o menu da interface?" --ok-label="Sim" --cancel-label="Não")
    if [ $? -eq 0 ]; then
        return 0
    else
        return 1
    fi
}

# Verifica se o zenity está instalado
if ! command -v zenity &> /dev/null; then
    echo "O Zenity não está instalado. Por favor, instale-o antes de continuar."
    exit 1
fi

# Obter confirmação do usuário
obter_confirmacao
confirmacao=$?

# Verificar resposta do usuário
if [ $confirmacao -eq 0 ]; then
    while true; do
        # Chama o script da interface gráfica
        escolha=$(zenity --list --title="Escolha uma opção" --column="Opção" "Modificar permissões de arquivos" "Listar diretórios do Linux !" "Copiar arquivos entre máquinas" "Sair")

        case $escolha in
            "Modificar permissões de arquivos")
                # Chama o script para modificar permissões
                alvo=$(zenity --entry --title="Modificar Permissões" --text="Informe o diretório ou arquivo alvo:")
                exemplo_permissao="Exemplo: 755"
                permissao=$(zenity --entry --title="Modificar Permissões" --text="Informe as permissões desejadas (em formato octal, $exemplo_permissao):")

                # Exemplo para facilitar a interpretação do usuário
                zenity --info --title="Exemplo" --text="Exemplo: $ alvo\n\n$ chmod $permissao $alvo" --width=400
                sleep 3

                # Verifica se o alvo existe
                if [ ! -e "$alvo" ]; then
                    zenity --error --title="Erro" --text="O diretório ou arquivo especificado não existe."
                    continue
                fi

                # Adicione aqui o código para modificar as permissões
                chmod "$permissao" "$alvo"

                zenity --info --title="Sucesso" --text="Permissões modificadas com sucesso!" --width=200
                ;;

            "Listar diretórios do Linux !")
                # Chama o script para listar diretórios
                diretorio=$(zenity --entry --title="Listar Diretórios" --text="Informe o diretório a ser explicado, como /boot! :")

                # Verifica se o diretório é um dos diretórios específicos mencionados
                case $diretorio in
                    /)
                        descricao="Diretório raiz, contendo todos os demais diretórios."
                        ;;
                    /bin)
                        descricao="Contém arquivos programas do sistema que são usados com frequência pelos usuários."
                        ;;
                    /boot)
                        descricao="Arquivos estáticos e gerenciador de inicialização."
                        ;;
                    /dev)
                        descricao="Arquivos de dispositivos (periféricos)."
                        ;;
                    /etc)
                        descricao="Arquivos de configuração do sistema, específicos da máquina."
                        ;;
                    /home)
                        descricao="Contém os diretórios dos usuários."
                        ;;
                    /lib)
                        descricao="Bibliotecas essenciais compartilhadas e módulos do kernel."
                        ;;
                    /mnt)
                        descricao="Ponto de montagem para montar um sistema de arquivos temporariamente."
                        ;;
                    /proc)
                        descricao="Diretório virtual de informações do sistema."
                        ;;
                    /root)
                        descricao="Diretório home do usuário root."
                        ;;
                    /sbin)
                        descricao="Diretório de programas usados pelo superusuário root, para administração e controle do funcionamento do sistema."
                        ;;
                    /tmp)
                        descricao="Arquivos temporários."
                        ;;
                    /usr)
                        descricao="Contém a maior parte de seus programas. Normalmente acessível somente como leitura."
                        ;;
                    /var)
                        descricao="Dados variáveis, como arquivos e diretórios de spool, dados de administração e login, e arquivos transitórios."
                        ;;
                    /opt)
                        descricao="Aplicativos adicionais e pacotes de softwares."
                        ;;
                    *)
                        zenity --error --title="Erro" --text="Diretório não reconhecido ou não permitido."
                        continue
                        ;;
                esac

                # Adiciona a descrição do diretório à listagem
                listagem=$(ls "$diretorio" 2>/dev/null)
                zenity --info --title="Conteúdo do Diretório $diretorio" --text="$descricao\n\nConteúdo do Diretório:\n$listagem" --width=400 --height=300
                ;;

            "Copiar arquivos entre máquinas")
                # Chama o script para copiar arquivos entre máquinas
                origem=$(zenity --entry --title="Copiar Arquivos" --text="Informe o caminho do arquivo de origem:")
                destino=$(zenity --entry --title="Copiar Arquivos" --text="Informe o caminho do diretório de destino:")

                # Verifica se o arquivo de origem existe
                if [ ! -e "$origem" ]; then
                    zenity --error --title="Erro" --text="O arquivo de origem especificado não existe."
                    continue
                fi

                # Adicione aqui o código para realizar a cópia usando o scp
                # Substitua user@remote_host pelo seu valor real
                scp "$origem" user@remote_host:"$destino"

                zenity --info --title="Sucesso" --text="Arquivo copiado com sucesso!" --width=200
                ;;

            "Sair")
                echo "Saindo..."
                exit 0
                ;;

            *)
                zenity --error --title="Erro" --text="Escolha inválida. Tente novamente."
                ;;
        esac
    done
else
    echo "Saindo..."
    exit 0
fi
