#!/bin/bash#!/bin/bash

# Verificar se o yad está instalado
if ! command -v yad &> /dev/null; then
  echo "O yad não está instalado. Tentando instalar..."
  
  # Tenta instalar o yad usando o apt
  if sudo apt-get update && sudo apt-get install -y yad; then
    echo "O yad foi instalado com sucesso."
  else
    echo "Não foi possível instalar o yad. Por favor, instale-o com sudo apt install yad"
    exit 1
  fi
fi


# Diálogo de entrada para obter informações do usuário
user_info=$(yad --form --title="Detalhes de Conexão" --text="Bem-vindo ao script de compartilhamento:" \
	--field="Nome de Usuário e o endereço de IP (user@ip):" \
  --field="Caminho do Arquivo de Origem:" \
  --field="Caminho de Destino no Servidor Remoto:" \
  "" "" "" "")

# Converter a string retornada pelo yad em variáveis
IFS="|" read -r username destination_ip source_file destination_path <<< "$user_info"

# Construa o comando SCP
scp "$source_file" "$username@$destination_ip:$destination_path"

# Verifique se a transferência foi bem-sucedida
if [ $? -eq 0 ]; then
  yad --info --title="Sucesso" --text="Arquivo transferido com sucesso para $destination_ip:$destination_path"
else
  yad --error --title="Erro" --text="Ocorreu um erro durante a transferência."
fi

